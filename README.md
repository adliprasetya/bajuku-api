# Bajuku-API

Available API For Now 

**Users**
API | Function
---|-----
api/users/register| Register user from page
api/users/delete{id} | Delete user by `id`
api/users/update{id} | Update user field by `id`
api/users/login | Login with indentifying password by hash
api/users/verifyemail/{token} | Verification user account by Token

**Products**
API | Function
----|-------
api/products/add | Add products
api/products/getproduct{limit} | Get records of products by limit
api/products/getcategory{id} | Get records of products by category
api/products/delete{id} | Delete product by `id`
api/products/getproductsearch | Search by keywords (optional), category (optional), and sort (optional)



## Preparation

**Create Database**
```sql
CREATE DATABASE apitest
```

**Table Product**
```sql
CREATE TABLE products(
	id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100),
    price INT,
	description TEXT,
	keywords VARCHAR(150),
	fk_category_id INT,
	image_content MEDIUMTEXT,
	stok INT,
	entry_date DATE
);
```

**Table *Users***
```sql
CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255),
    password_hash VARBINARY(256),
	password_salt VARBINARY(256),
    first_name VARCHAR(50),
    last_name VARCHAR(100),
	id_role INT,
	active BOOLEAN
)
```