namespace Bajuku_API.Models;

public class Users
{
  public int? id { get; set; }
  public string? email { get; set; }
  public byte[]? password_hash { get; set; }
  public byte[]? password_salt { get; set; }
  public string? first_name { get; set; }
  public string? last_name { get; set; }
  public int? id_role { get; set; }
}

public class UsersBody
{
  public int? id { get; set; }
  public string? email { get; set; }
  public string? password { get; set; }
  public string? first_name { get; set; }
  public string? last_name { get; set; }
  public int? id_role { get; set; }

}

public class UsersLogin
{
    public string email { get; set; }
    public string password { get; set; }
}

public class UsersDelete
{
    public string id { get; set; }
}

public class UsersUpdate
{ 
  public int id { get; set; }
  public string email { get; set; }
  public string username { get; set; }
  public string first_name { get; set; }
  public string last_name { get; set; } 
  public string address { get; set; }
  public string telephone { get; set; }
  public int id_role { get; set; }

}