using Microsoft.AspNetCore.Http;
namespace Bajuku_API.Models;

public class orderInvoice
{
    public int id { get; set; }
    public int id_user { get; set; }
    public string? tanggal_beli { get; set; }
    public string? nama { get; set; }
    public string? alamat { get; set; }
    public string? customer_name { get; set; }
    public string? credit_card { get; set; }
    public string? telephone { get; set; }
    public int total_quantity { get; set; }
    public int total_price { get; set; }
}

public class orderInvoiceCart
{
    public int? id { get; set; }
    public int? id_user { get; set; }
    // public string? tanggal_beli { get; set; }
    public string? nama { get; set; }
    public string? alamat { get; set; }
    public string? customer_name { get; set; }
    public string? credit_card { get; set; }
    public string? telephone { get; set; }
    public int? total_quantity { get; set; }
    public int? total_price { get; set; }
    public List<int> list_product { get; set; }
    public List<int> list_quantity { get; set; }
}

public class orderItem
{
    public int id { get; set; }
    public int id_product { get; set; }
    public int quantity { get; set; }
    public int id_invoice { get; set; }
}

public class orderListItem
{
    public string? product_title { get; set; }
    public string? category { get; set; }
    public int quantity { get; set; }
    public int price { get; set; }
}

public class orderInvoiceAdmin
{
    public int? id { get; set; }
    public int? id_user { get; set; }
    public string? nama { get; set; }
    public string? alamat { get; set; }
    public string? customer_name { get; set; }
    public string? credit_card { get; set; }
    public string? telephone { get; set; }
    public int? total_quantity { get; set; }
    public int? total_price { get; set; }
    public string? status { get; set; }
}
