using Microsoft.AspNetCore.Http;
namespace Bajuku_API.Models;

public class Products
{

    public int id { get; set; }
    public string? title { get; set; }
    public string? details { get; set; }
    public string image_content { get; set; }
    public int id_category { get; set; }
    public int price { get; set; }
}

public class ProductBody
{
    public int id { get; set; }
    public string? title { get; set; }
    public int price { get; set; }
}

public class ProductList
{
    public int? id { get; set; }
    public string? title { get; set; }
    public int? price { get; set; }
    public string? description { get; set; }
    public string? keywords { get; set; }
    public int? fk_category_id { get; set; }
    public string? image_content { get; set; }
    public int? stok { get; set; }
}

public class Update
{
    public string? title { get; set; }
    public int? price { get; set; }
    public string? description { get; set; }
    public string? keywords { get; set; }
    public int? fk_category_id { get; set; }
    public string? image_content { get; set; }
    public int? stok { get; set; }
}

public class ProductSearch
{
    public string? search { get; set; }
    public List<int>? category { get; set; }
    public string? sort { get; set; }
    public int? maxPrice { get; set; }
    public int? minPrice { get; set; }

}

public class ProductSearchCategory {
  public string? search { get; set; }
  public string? sort { get; set; }
  public int? maxPrice { get; set; }
  public int? minPrice { get; set; }

}

public class ProductRecomend {
  public int? id { get; set; }
  public string? title { get; set; }
  public string? name { get; set; }
  public int? price { get; set; }
  public string? description { get; set; }
  public string? image_content { get; set; }
  public int? stok { get; set; }
}