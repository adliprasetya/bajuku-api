namespace BelajarWeb.Models
{
    public class OrderInvoice
    {
        public int? id { get; set; }
        public int? product_id { get; set; }
        public int? order_invoice_id { get; set; }
        public int? user_id { get; set; }
        public string? name { get; set; }
        public string? phone { get; set; }
        public string? payment_method { get; set; }
        public int? quantity { get; set; }
        public double? price { get; set; }
        public string? address { get; set; }
        public string? status { get; set; }
    }
}