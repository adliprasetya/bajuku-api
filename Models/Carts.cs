using Microsoft.AspNetCore.Http;
namespace Bajuku_API.Models;

public class Carts
{
	public int id_user { get; set; }
	public int id_product { get; set; }
	public int quantity { get; set; }	
}

public class DeleteCarts
{
	public int id_user { get; set; }
	public List<int> id_product { get; set; }
}