using BelajarWeb.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Bajuku_API.Models;
using System.Data;
using System.Text.RegularExpressions;


namespace Bajuku_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private IConfiguration _configuration;
    private string _conString = "";

    public ProductsController(IConfiguration configuration)
    {
        _configuration = configuration;
        _conString = configuration["ConnectionStrings:Default"];
    }

    [HttpGet]
    [Route("GetData")]

    public ActionResult GetData()
    {
        return StatusCode(200, "Hi Saya dari product controller");
    }

    [HttpPost]
    [Route("Add")]

    public ActionResult Add([FromBody] ProductList product)
    {
        try
        {


            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    cmd.CommandText = "INSERT INTO Product (id, title, price, description, keywords, fk_category_id, image_content, stok, entry_date) VALUE(@id, @title, @price, @description, @keywords, @fk_category_id, @image_content, @stok, @entry_date)";

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = product.id },
            new MySqlParameter { ParameterName = "@title", MySqlDbType = MySqlDbType.String, Value = product.title },
            new MySqlParameter { ParameterName = "@price", MySqlDbType = MySqlDbType.Int32, Value = product.price },
            new MySqlParameter { ParameterName = "@description", MySqlDbType = MySqlDbType.String, Value = product.description },
            new MySqlParameter { ParameterName = "@keywords", MySqlDbType = MySqlDbType.String, Value = product.keywords },
            new MySqlParameter { ParameterName = "@fk_category_id", MySqlDbType = MySqlDbType.Int32, Value = product.fk_category_id },
            new MySqlParameter { ParameterName = "@image_content", MySqlDbType = MySqlDbType.String, Value = product.image_content },
            new MySqlParameter { ParameterName = "@stok", MySqlDbType = MySqlDbType.Int32, Value = product.stok },
            new MySqlParameter { ParameterName = "@entry_date", MySqlDbType = MySqlDbType.Date, Value = DateTime.Now.Date }
                    });

                    try
                    {
                        int res = cmd.ExecuteNonQuery();
                        if (res == -1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                        }
                    }
                    catch
                    {
                        cmd.Transaction.Rollback();
                        throw;
                    }

                }

                conn.Close();
            }

            return StatusCode(200, "Data Product Berhasil disimpan!");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("Edite")]

    public ActionResult Edite([FromBody] Update product, int id)
    {
        try
        {

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    cmd.CommandText = "UPDATE Product SET title = @title, price = @price, description = @description, keywords = @keywords, fk_category_id = @fk_category_id, image_content = @image_content, stok = @stok WHERE id = @id";

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = id },
            new MySqlParameter { ParameterName = "@title", MySqlDbType = MySqlDbType.String, Value = product.title },
            new MySqlParameter { ParameterName = "@price", MySqlDbType = MySqlDbType.Int32, Value = product.price },
            new MySqlParameter { ParameterName = "@description", MySqlDbType = MySqlDbType.String, Value = product.description },
            new MySqlParameter { ParameterName = "@keywords", MySqlDbType = MySqlDbType.String, Value = product.keywords },
            new MySqlParameter { ParameterName = "@fk_category_id", MySqlDbType = MySqlDbType.Int32, Value = product.fk_category_id },
            new MySqlParameter { ParameterName = "@image_content", MySqlDbType = MySqlDbType.String, Value = product.image_content },
            new MySqlParameter { ParameterName = "@stok", MySqlDbType = MySqlDbType.Int32, Value = product.stok },
                    });

                    try
                    {
                        int res = cmd.ExecuteNonQuery();
                        if (res == -1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                        }
                    }
                    catch
                    {
                        cmd.Transaction.Rollback();
                        throw;
                    }

                }

                conn.Close();
            }

            return StatusCode(200, "Data Product Berhasil diubah!");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet]
    [Route("GetProduct")]

    public ActionResult GetProduct(int limit)
    {
        try
        {
            var result = new List<ProductList>();
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM Product LIMIT @limit";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@limit", DbType = DbType.Int32, Value = limit }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var tempProduct = new ProductList();

                            tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            tempProduct.title = reader["title"] == DBNull.Value ? "" : (string)reader["title"];
                            tempProduct.price = reader["price"] == DBNull.Value ? 0 : (int)reader["price"];
                            tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                            tempProduct.keywords = reader["keywords"] == DBNull.Value ? "" : (string)reader["keywords"];
                            tempProduct.fk_category_id = reader["fk_category_id"] == DBNull.Value ? 0 : (int)reader["fk_category_id"];
                            tempProduct.image_content = reader["image_content"] == DBNull.Value ? "" : (string)reader["image_content"];
                            tempProduct.stok = reader["stok"] == DBNull.Value ? 0 : (int)reader["stok"];

                            result.Add(tempProduct);

                        }
                    }

                    reader.Close();
                }
                conn.Close();
            }

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
            throw;
        }
    }

    [HttpGet]
    [Route("GetCategory")]

    public ActionResult GetCategory(int id)
    {
        try
        {
            var result = new List<ProductBody>();
            bool isNotMatch = false;

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT id, title, price FROM products WHERE id_category = @id;";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@id", DbType = DbType.Int32, Value = id }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var tempProduct = new ProductBody();

                            tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            tempProduct.title = reader["title"] == DBNull.Value ? "" : (string)reader["title"];
                            tempProduct.price = reader["price"] == DBNull.Value ? 0 : (int)reader["price"];

                            result.Add(tempProduct);

                        }
                    }
                    else
                    {
                        isNotMatch = true;
                    }

                    reader.Close();
                }
                conn.Close();
            }

            if (isNotMatch)
            {
                return StatusCode(200, "Barang tidak ditemukan");
            }

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
            // throw;
        }
    }


    [HttpDelete]
    [Route("Delete")]

    public ActionResult Delete(int id)
    {
        try
        {

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandText = "DELETE FROM Product WHERE id = @id;";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@id", DbType = DbType.Int32, Value = id }
                    );

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            return StatusCode(200, "Success to delete product by id: " + id);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
            throw;
        }
    }

    [HttpGet]
    [Route("GetProductListing")]

    public ActionResult GetProductListing()
    {
        try
        {
            var result = new List<ProductRecomend>();
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT Product.id, Product.title, Category.name, Product.price, Product.description, Product.image_content FROM Product INNER JOIN Category ON Category.id = Product.fk_category_id ORDER BY RAND() LIMIT @limit;";

                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@limit", DbType = DbType.Int32, Value = 5 }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var tempProduct = new ProductRecomend();

                            tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            tempProduct.title = reader["title"] == DBNull.Value ? "" : (string)reader["title"];
                            tempProduct.name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                            tempProduct.price = reader["price"] == DBNull.Value ? 0 : (int)reader["price"];
                            tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                            tempProduct.image_content = reader["image_content"] == DBNull.Value ? "" : (string)reader["image_content"];

                            result.Add(tempProduct);

                        }
                    }

                    reader.Close();
                }
                conn.Close();
            }

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
            throw;
        }
    }

    [HttpPost]
    [Route("GetProductSearch")]

    public ActionResult GetProductSearch([FromBody] ProductSearch product)
    {
        try
        {
            DataTable table = new DataTable();
            string specSearch = "";



            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    string query = "SELECT * FROM Product";

                    // Search Product
                    if (!String.IsNullOrEmpty(product.search))
                    {
                        query += " WHERE";


                        specSearch = Regex.Replace(product.search, @"\s+", " ");
                        specSearch = specSearch.Trim();
                        specSearch = specSearch.Replace(" ", "|");

                        query += " (`title` REGEXP @search OR `keywords` REGEXP @search)";
                    }

                    // Search by Category
                    if (product.category.Count != 0)
                    {
                        int i = 0;
                        if (!String.IsNullOrEmpty(product.search))
                        {
                            query += " AND";
                        }
                        else
                        {
                            query += " WHERE";
                        }

                        query += " fk_category_id IN (";
                        foreach (var item in product.category)
                        {
                            i++;

                            query += "@category" + i.ToString();
                            cmd.Parameters.Add(
                              new MySqlParameter { ParameterName = "@category" + i.ToString(), MySqlDbType = MySqlDbType.Int32, Value = item }
                            );

                            if (i != product.category.Count)
                            {
                                query += ",";
                            }
                        }

                        query += ")";
                    }

                    // Sort by MaxPrice and MinPrice

                    if (product.maxPrice != null || product.minPrice != null)
                    {
                        if (!String.IsNullOrEmpty(product.search) || product.category.Count != 0)
                        {
                            query += " AND";
                        }
                        else
                        {
                            query += " WHERE";
                        }

                        query += " price >= @min_price";

                        if (product.maxPrice != 0 && product.maxPrice != null && product.maxPrice > product.minPrice)
                        {
                            query += " AND price <= @max_price";
                        }
                    }

                    // Sorting

                    switch (product.sort)
                    {
                        case "new":
                            query += " ORDER BY entry_date DESC";
                            break;
                        case "old":
                            query += " ORDER BY entry_date ASC";
                            break;
                        case "expensive":
                            query += " ORDER BY price DESC";
                            break;
                        case "cheapest":
                            query += " ORDER BY price ASC";
                            break;
                    }

                    cmd.CommandText = query;
                    cmd.Connection = conn;
                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter{ParameterName = "@search", MySqlDbType = MySqlDbType.String, Value = specSearch},
            new MySqlParameter{ParameterName = "@max_price", MySqlDbType = MySqlDbType.Int32, Value = product.maxPrice < product.minPrice ? 1 + product.minPrice : product.maxPrice },
            new MySqlParameter{ParameterName = "@min_price", MySqlDbType = MySqlDbType.Int32, Value = product.minPrice == null ? 0 : product.minPrice}
                    }
                    );


                    MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                    adapter.Fill(table);
                }
                conn.Close();


                List<object> result = new List<object>();
                foreach (DataRow row in table.Rows)
                {

                    var temp = new
                    {
                        id = row["id"] == DBNull.Value ? 0 : (int)row["id"],
                        title = row["title"] == DBNull.Value ? "" : (string)row["title"],
                        price = row["price"] == DBNull.Value ? 0 : (int)row["price"],
                        description = row["description"] == DBNull.Value ? "" : (string)row["description"],
                        keywords = row["keywords"] == DBNull.Value ? "" : (string)row["keywords"],
                        fk_category_id = row["fk_category_id"] == DBNull.Value ? 0 : (int)row["fk_category_id"],
                        image_content = row["image_content"] == DBNull.Value ? "" : (string)row["image_content"],
                        stok = row["stok"] == DBNull.Value ? 0 : (int)row["stok"]
                    };

                    result.Add(temp);
                }

                return StatusCode(200, result);
            }
        }
        catch (System.Exception)
        {

            throw;
        }
    }


    [HttpPost]
    [Route("GetProductSearchByCategory")]

    public ActionResult GetProductSearchByCategory([FromBody] ProductSearchCategory product, int id_category)
    {
        try
        {
            DataTable table = new DataTable();
            string specSearch = "";

            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    string query = "SELECT * FROM Product WHERE fk_category_id = @id";

                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = id_category }
                    );

                    // Search Product
                    if (!String.IsNullOrEmpty(product.search))
                    {
                        query += " AND";


                        specSearch = Regex.Replace(product.search, @"\s+", " ");
                        specSearch = specSearch.Trim();
                        specSearch = specSearch.Replace(" ", "|");

                        query += " (`title` REGEXP @search OR `keywords` REGEXP @search)";
                    }

                    // Sort by MaxPrice and MinPrice

                    if (product.maxPrice != null || product.minPrice != null)
                    {
                        query += " AND price >= @min_price";

                        if (product.maxPrice != 0 && product.maxPrice != null && product.maxPrice > product.minPrice)
                        {
                            query += " AND price <= @max_price";
                        }
                    }

                    // Sorting

                    switch (product.sort)
                    {
                        case "new":
                            query += " ORDER BY entry_date DESC";
                            break;
                        case "old":
                            query += " ORDER BY entry_date ASC";
                            break;
                        case "expensive":
                            query += " ORDER BY price DESC";
                            break;
                        case "cheapest":
                            query += " ORDER BY price ASC";
                            break;
                    }

                    cmd.CommandText = query;
                    cmd.Connection = conn;
                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter{ParameterName = "@search", MySqlDbType = MySqlDbType.String, Value = specSearch},
            new MySqlParameter{ParameterName = "@max_price", MySqlDbType = MySqlDbType.Int32, Value = product.maxPrice < product.minPrice ? 1 + product.minPrice : product.maxPrice },
            new MySqlParameter{ParameterName = "@min_price", MySqlDbType = MySqlDbType.Int32, Value = product.minPrice == null ? 0 : product.minPrice}
                    }
                    );


                    MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                    adapter.Fill(table);
                }
                conn.Close();


                List<object> result = new List<object>();
                foreach (DataRow row in table.Rows)
                {

                    var temp = new
                    {
                        id = row["id"] == DBNull.Value ? 0 : (int)row["id"],
                        title = row["title"] == DBNull.Value ? "" : (string)row["title"],
                        price = row["price"] == DBNull.Value ? 0 : (int)row["price"],
                        description = row["description"] == DBNull.Value ? "" : (string)row["description"],
                        keywords = row["keywords"] == DBNull.Value ? "" : (string)row["keywords"],
                        fk_category_id = row["fk_category_id"] == DBNull.Value ? 0 : (int)row["fk_category_id"],
                        image_content = row["image_content"] == DBNull.Value ? "" : (string)row["image_content"],
                        stok = row["stok"] == DBNull.Value ? 0 : (int)row["stok"]
                    };

                    result.Add(temp);
                }

                return StatusCode(200, result);
            }
        }
        catch (System.Exception)
        {

            throw;
        }
    }

}