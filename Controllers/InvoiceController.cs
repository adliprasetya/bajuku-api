using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System.Data;
using BelajarWeb.Models;
using Bajuku_API.Models;


namespace BelajarWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private IConfiguration _configuration;
        private string _conString = "";

        public InvoiceController(IConfiguration configuration)
        {
            _configuration = configuration;
            _conString = configuration["ConnectionStrings:Default"];
        }

        [HttpGet]
        [Route("getinvoicetable")]
        public ActionResult GetInvoiceTable()
        {
            try
            {
                DataTable invoiceTable = new DataTable();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT * FROM order_invoice_items";

                        MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                        adapter.Fill(invoiceTable);
                    }

                    conn.Close();
                }

                List<object> result = new List<object>();

                foreach (DataRow row in invoiceTable.Rows)
                {
                    var temp = new
                    {
                        id = row["id"] == DBNull.Value ? 0 : (int)row["id"],
                        user_id = row["user_id"] == DBNull.Value ? 0 : (int)row["user_id"],
                        order_invoice_id = row["order_invoice_id"] == DBNull.Value ? 0 : (int)row["order_invoice_id"],
                        name = row["name"] == DBNull.Value ? "" : (string)row["name"],
                        quantity = row["quantity"] == DBNull.Value ? 0 : (int)row["quantity"],
                        total_price = row["total_price"] == DBNull.Value ? 0 : (double)row["total_price"],
                        status = row["status"] == DBNull.Value ? "" : (string)row["status"]
                    };

                    result.Add(temp);
                }

                return StatusCode(200, result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        public ActionResult UpdateStatus(int id, string status)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE Order_invoice SET `status` = @status WHERE id = @id";
                        cmd.Parameters.AddRange(
                            new MySqlParameter[]
                            {
                                new MySqlParameter {ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = id},
                                new MySqlParameter {ParameterName = "@status", MySqlDbType = MySqlDbType.String, Value = status}
                            });

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (System.Exception ex)
                        {
                            return BadRequest(ex.Message);
                        }
                    }

                    conn.Close();
                }

                return StatusCode(200);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("postdatainvoiceuser")]
        public ActionResult PostDataInvoiceUser([FromBody] OrderInvoice invoiceUser)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "INSERT INTO order_invoices VALUES (@id, @product_id, @order_invoice_id, @user_id, @name, @customer_phone, @payment_method, @quantity, @total_price, @address, @status)";
                        cmd.Parameters.AddRange(
                            new MySqlParameter[]
                            {
                                new MySqlParameter {ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.id},
                                new MySqlParameter {ParameterName = "@product_id", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.product_id},
                                new MySqlParameter {ParameterName = "@order_invoice_id", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.order_invoice_id},
                                new MySqlParameter {ParameterName = "@user_id", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.user_id},
                                new MySqlParameter {ParameterName = "@name", MySqlDbType = MySqlDbType.String, Value = invoiceUser.name},
                                new MySqlParameter {ParameterName = "@customer_phone", MySqlDbType = MySqlDbType.String, Value = invoiceUser.phone},
                                new MySqlParameter {ParameterName = "@payment_method", MySqlDbType = MySqlDbType.String, Value = invoiceUser.payment_method},
                                new MySqlParameter {ParameterName = "@quantity", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.quantity},
                                new MySqlParameter {ParameterName = "@total_price", MySqlDbType = MySqlDbType.Int32, Value = invoiceUser.price},
                                new MySqlParameter {ParameterName = "@address", MySqlDbType = MySqlDbType.String, Value = invoiceUser.address},
                                new MySqlParameter {ParameterName = "@status", MySqlDbType = MySqlDbType.String, Value = invoiceUser.status},
                            });

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (System.Exception ex)
                        {
                            return BadRequest(ex.Message);
                        }
                    }

                    conn.Close();
                }

                return StatusCode(200);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("AddInvoice")]

        public ActionResult AddInvoice([FromBody] orderInvoice invoice, int id_product, int quantity)
        {
            try
            {
                int result = 0;

                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "INSERT INTO Order_invoice (id, id_user, tanggal_beli, nama, alamat, customer_name, credit_card, telephone, total_quantity, total_price, status) VALUE (@id, @id_user, @tanggal_beli, @nama, @alamat, @customer_name, @credit_card, @telephone, @total_quantity, @total_price, 'Pending')";

                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                           new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = invoice.id },
                           new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = invoice.id_user },
                           new MySqlParameter { ParameterName = "@tanggal_beli", MySqlDbType = MySqlDbType.Date, Value = DateTime.Now.Date },
                           new MySqlParameter { ParameterName = "@nama", MySqlDbType = MySqlDbType.String, Value = invoice.nama },
                           new MySqlParameter { ParameterName = "@alamat", MySqlDbType = MySqlDbType.String, Value = invoice.alamat },
                           new MySqlParameter { ParameterName = "@customer_name", MySqlDbType = MySqlDbType.String, Value = invoice.customer_name },
                           new MySqlParameter { ParameterName = "@credit_card", MySqlDbType = MySqlDbType.String, Value = invoice.credit_card },
                           new MySqlParameter { ParameterName = "@telephone", MySqlDbType = MySqlDbType.String, Value = invoice.telephone },
                           new MySqlParameter { ParameterName = "@total_quantity", MySqlDbType = MySqlDbType.Int32, Value = invoice.total_quantity },
                           new MySqlParameter { ParameterName = "@total_price", MySqlDbType = MySqlDbType.Int32, Value = invoice.total_price },
                        });

                        try
                        {
                            int res = cmd.ExecuteNonQuery();
                            if (res == -1)
                            {
                                cmd.Transaction.Rollback();
                            }
                            else
                            {
                                cmd.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            cmd.Transaction.Rollback();
                            throw;
                        }
                    }



                    using (var id = new MySqlCommand())
                    {
                        id.Connection = conn;
                        id.CommandText = "SELECT id FROM Order_invoice ORDER BY id DESC LIMIT 1";

                        MySqlDataReader reader = id.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            }
                        }
                        reader.Close();
                    }

                    using (var invoiceItem = new MySqlCommand())
                    {
                        invoiceItem.Connection = conn;
                        invoiceItem.Transaction = conn.BeginTransaction();
                        invoiceItem.CommandText = "INSERT INTO Order_item (id, id_product, quantity, id_invoice) VALUE (@id, @id_product, @quantity, @id_invoice)";

                        invoiceItem.Parameters.AddRange(new MySqlParameter[]
                        {
                           new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = 0 },
                           new MySqlParameter { ParameterName = "@id_product", MySqlDbType = MySqlDbType.Int32, Value = id_product },
                            new MySqlParameter { ParameterName = "@quantity", MySqlDbType = MySqlDbType.Int32, Value = quantity },
                             new MySqlParameter { ParameterName = "@id_invoice", MySqlDbType = MySqlDbType.Int32, Value = result },
                        });

                        try
                        {
                            int res = invoiceItem.ExecuteNonQuery();
                            if (res == -1)
                            {
                                invoiceItem.Transaction.Rollback();
                            }
                            else
                            {
                                invoiceItem.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            invoiceItem.Transaction.Rollback();
                            throw;
                        }
                    }

                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("AddInvoiceCart")]

        public ActionResult AddInvoiceCart([FromBody] orderInvoiceCart invoice)
        {
            try
            {
                int result = 0;

                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "INSERT INTO Order_invoice (id, id_user, tanggal_beli, nama, alamat, customer_name, credit_card, telephone, total_quantity, total_price, status) VALUE (@id, @id_user, @tanggal_beli, @nama, @alamat, @customer_name, @credit_card, @telephone, @total_quantity, @total_price, 'Pending')";

                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                           new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = invoice.id },
                           new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = invoice.id_user },
                           new MySqlParameter { ParameterName = "@tanggal_beli", MySqlDbType = MySqlDbType.Date, Value = DateTime.Now.Date },
                           new MySqlParameter { ParameterName = "@nama", MySqlDbType = MySqlDbType.String, Value = invoice.nama },
                           new MySqlParameter { ParameterName = "@alamat", MySqlDbType = MySqlDbType.String, Value = invoice.alamat },
                           new MySqlParameter { ParameterName = "@customer_name", MySqlDbType = MySqlDbType.String, Value = invoice.customer_name },
                           new MySqlParameter { ParameterName = "@credit_card", MySqlDbType = MySqlDbType.String, Value = invoice.credit_card },
                           new MySqlParameter { ParameterName = "@telephone", MySqlDbType = MySqlDbType.String, Value = invoice.telephone },
                           new MySqlParameter { ParameterName = "@total_quantity", MySqlDbType = MySqlDbType.Int32, Value = invoice.total_quantity },
                           new MySqlParameter { ParameterName = "@total_price", MySqlDbType = MySqlDbType.Int32, Value = invoice.total_price },
                        });

                        try
                        {
                            int res = cmd.ExecuteNonQuery();
                            if (res == -1)
                            {
                                cmd.Transaction.Rollback();
                            }
                            else
                            {
                                cmd.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            cmd.Transaction.Rollback();
                            throw;
                        }
                    }



                    using (var id = new MySqlCommand())
                    {
                        id.Connection = conn;
                        id.CommandText = "SELECT id FROM Order_invoice ORDER BY id DESC LIMIT 1";

                        MySqlDataReader reader = id.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            }
                        }
                        reader.Close();
                    }

                    for (var i = 0; i < invoice.list_product.Count; i++)
                    {
                        using (var cmd = new MySqlCommand())
                        {

                            try
                            {
                                cmd.CommandText = "INSERT INTO Order_item (id_product, quantity, id_invoice) VALUES (@id_product, @quantity, @id_invoice);";
                                cmd.Connection = conn;
                                cmd.Parameters.AddRange(new MySqlParameter[] {
                                    new MySqlParameter { ParameterName = "@id_product", MySqlDbType = MySqlDbType.Int32, Value = invoice.list_product[i] },
                                    new MySqlParameter { ParameterName = "@quantity", MySqlDbType = MySqlDbType.Int32, Value = invoice.list_quantity[i] },
                                    new MySqlParameter { ParameterName = "@id_invoice", MySqlDbType = MySqlDbType.Int32, Value = result },
                                });

                                cmd.ExecuteNonQuery();
                            }
                            catch (System.Exception ex)
                            {
                                throw;
                            }
                        }
                    }

                    conn.Close();
                }
                return StatusCode(200, "Transaksi Berhasil");
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("GetInvoice")]
        public ActionResult GetInvoice()
        {
            try
            {
                var result = new List<orderInvoice>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT * FROM Order_invoice ORDER BY id DESC";

                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var temp = new orderInvoice();

                                temp.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                temp.id_user = reader["id_user"] == DBNull.Value ? 0 : (int)reader["id_user"];
                                temp.tanggal_beli = reader["tanggal_beli"] == DBNull.Value ? "" : Convert.ToDateTime(reader["tanggal_beli"]).ToString("dd/MM/yyyy");
                                temp.nama = reader["nama"] == DBNull.Value ? "" : (string)reader["nama"];
                                temp.alamat = reader["alamat"] == DBNull.Value ? "" : (string)reader["alamat"];
                                temp.customer_name = reader["customer_name"] == DBNull.Value ? "" : (string)reader["customer_name"];
                                temp.credit_card = reader["credit_card"] == DBNull.Value ? "" : (string)reader["credit_card"];
                                temp.telephone = reader["telephone"] == DBNull.Value ? "" : (string)reader["telephone"];
                                temp.total_quantity = reader["total_quantity"] == DBNull.Value ? 0 : (int)reader["total_quantity"];
                                temp.total_price = reader["total_price"] == DBNull.Value ? 0 : (int)reader["total_price"];

                                result.Add(temp);
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("AddItem")]

        public ActionResult AddItem([FromBody] orderItem invoice)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "INSERT INTO Order_item (id, id_product, quantity, id_invoice) VALUE (@id, @id_product, @quantity, @id_invoice)";

                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                           new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = invoice.id },
                           new MySqlParameter { ParameterName = "@id_product", MySqlDbType = MySqlDbType.Int32, Value = invoice.id_product },
                            new MySqlParameter { ParameterName = "@quantity", MySqlDbType = MySqlDbType.Int32, Value = invoice.quantity },
                             new MySqlParameter { ParameterName = "@id_invoice", MySqlDbType = MySqlDbType.Int32, Value = invoice.id_invoice },
                        });

                        try
                        {
                            int res = cmd.ExecuteNonQuery();
                            if (res == -1)
                            {
                                cmd.Transaction.Rollback();
                            }
                            else
                            {
                                cmd.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            cmd.Transaction.Rollback();
                            throw;
                        }
                    }
                    conn.Close();
                }
                return StatusCode(200, "Data Product Berhasil disimpan!");
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("GetItem")]
        public ActionResult GetItem(int infoId)
        {
            try
            {
                var result = new List<orderListItem>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT Product.title, Category.name, Order_item.quantity, Product.price FROM (((Order_item INNER JOIN Product ON Order_item.id_product = Product.id) INNER JOIN Category ON Product.fk_category_id = Category.id) INNER JOIN Order_invoice ON Order_item.id_invoice = Order_invoice.id) WHERE Order_item.id_invoice = @infoId";

                        cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@infoId", DbType = DbType.Int32, Value = infoId }
                        );

                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var temp = new orderListItem();

                                temp.product_title = reader["title"] == DBNull.Value ? "" : (string)reader["title"];
                                temp.category = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                                temp.quantity = reader["quantity"] == DBNull.Value ? 0 : (int)reader["quantity"];
                                temp.price = reader["price"] == DBNull.Value ? 0 : (int)reader["price"];

                                result.Add(temp);
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("GetInvoiceAdminTest")]
        public ActionResult GetInvoiceAdminTest()
        {
            try
            {
                var result = new List<orderInvoiceAdmin>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT * FROM Order_invoice";

                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var temp = new orderInvoiceAdmin();

                                temp.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                temp.id_user = reader["id_user"] == DBNull.Value ? 0 : (int)reader["id_user"];
                                temp.nama = reader["nama"] == DBNull.Value ? "" : (string)reader["nama"];
                                temp.alamat = reader["alamat"] == DBNull.Value ? "" : (string)reader["alamat"];
                                temp.customer_name = reader["customer_name"] == DBNull.Value ? "" : (string)reader["customer_name"];
                                temp.credit_card = reader["credit_card"] == DBNull.Value ? "" : (string)reader["credit_card"];
                                temp.telephone = reader["telephone"] == DBNull.Value ? "" : (string)reader["telephone"];
                                temp.total_quantity = reader["total_quantity"] == DBNull.Value ? 0 : (int)reader["total_quantity"];
                                temp.total_price = reader["total_price"] == DBNull.Value ? 0 : (int)reader["total_price"];
                                temp.status = reader["status"] == DBNull.Value ? "" : (string)reader["status"];

                                result.Add(temp);
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }
    }
}