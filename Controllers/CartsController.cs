using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using Bajuku_API.Models;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace Bajuku_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CartsController : ControllerBase
{
    private IConfiguration _configuration;
    private string _conString = "";

    public CartsController(IConfiguration configuration)
    {
        _configuration = configuration;
        _conString = configuration["ConnectionStrings:Default"];
    }

    [HttpGet]
    [Route("GetData")]
    [Authorize]

    public ActionResult GetData()
    {
        try
        {
            return StatusCode(200, "Berhasil");
        }
        catch (System.Exception ex)
        {
            return BadRequest("Gagal");
        }
    }

    [HttpPost]
    [Route("AddCart")]
    [Authorize]

    public ActionResult AddCart([FromBody] Carts cart)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();
                    cmd.CommandText = "INSERT INTO Cart (id_user, id_product, quantity) VALUES(@id_user, @id_product, @quantity);";

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
                          new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = cart.id_user },
                          new MySqlParameter { ParameterName = "@id_product", MySqlDbType = MySqlDbType.Int32, Value = cart.id_product },
                          new MySqlParameter { ParameterName = "@quantity", MySqlDbType = MySqlDbType.Int32, Value = cart.quantity }
                    });

                    int res = cmd.ExecuteNonQuery();

                    try
                    {
                        if (res == -1)
                        {
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                        }
                    }
                    catch
                    {
                        cmd.Transaction.Rollback();
                        throw;
                    }
                }

                conn.Close();
            }
            return StatusCode(200, "Berhasil Tambah Barang ke Cart");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("DeleteCart")]
    [Authorize]

    public ActionResult DeleteCart([FromBody] DeleteCarts cart)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    string query = "";
                    query = "DELETE FROM Cart WHERE id_product IN (";

                    int i = 0;

                    foreach (var item in cart.id_product)
                    {

                        i++;

                        query += "@id_product" + i.ToString();
                        cmd.Parameters.Add(
                          new MySqlParameter { ParameterName = "@id_product" + i.ToString(), MySqlDbType = MySqlDbType.Int32, Value = item }
                        );

                        if (i != cart.id_product.Count)
                        {
                            query += ",";
                        }
                    }

                    query += ") AND id_user = @id_user";
                    cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = cart.id_user }
                    );

                    cmd.CommandText = query;
                    cmd.Connection = conn;

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            return StatusCode(200, "Berhasil Hapus");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("DeleteCartByIdUser")]
    [Authorize]

    public ActionResult DeleteCartByIdUser(int id)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "DELETE FROM Cart WHERE id_user = @id_user;";

                    cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = id }
                    );

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
            return StatusCode(200, "Berhasil Delete Cart");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("DeleteOneCard")]

    public ActionResult DeleteOneCard(int id)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "DELETE FROM Cart WHERE id = @id";
                    cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = id }
                    );

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            return StatusCode(200, "Berhasil Hapus");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet]
    [Route("GetListingCart")]
    [Authorize]

    public ActionResult GetListingCart(int id)
    {

        try
        {
            DataTable table = new DataTable();

            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT Cart.id, Product.id as id_product, Category.name, Product.title, Product.image_content, Product.price, Cart.quantity FROM Cart INNER JOIN Product ON Cart.id_product = Product.id INNER JOIN Category ON Category.id = Product.fk_category_id WHERE id_user = @id_user;";

                    cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = id }
                    );

                    MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                    adapter.Fill(table);
                }

                conn.Close();
            }

            // Process Data
            List<object> result = new List<object>();

            foreach (DataRow row in table.Rows)
            {
                var temp = new
                {
                    id = row["id"] == DBNull.Value ? 0 : (int)row["id"],
                    id_product = row["id_product"] == DBNull.Value ? 0 : (int)row["id_product"],
                    category_name = row["name"] == DBNull.Value ? "" : (string)row["name"],
                    title = row["title"] == DBNull.Value ? "" : (string)row["title"],
                    image_content = row["image_content"] == DBNull.Value ? "" : (string)row["image_content"],
                    price = row["price"] == DBNull.Value ? 0 : (int)row["price"],
                    quantity = row["quantity"] == DBNull.Value ? 0 : (int)row["quantity"]
                };

                result.Add(temp);
            }

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet]
    [Route("GetCountCart")]
    [Authorize]

    public ActionResult GetCountCart(int id_user)
    {

        try
        {
            int count = 0;
            using (var conn = new MySqlConnection(_conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT COUNT(id) as result FROM Cart WHERE id_user = @id_user";

                    cmd.Parameters.Add(
                        new MySqlParameter { ParameterName = "@id_user", MySqlDbType = MySqlDbType.Int32, Value = id_user }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            count = (int)(long)reader["result"];
                        }
                    }
                }
                conn.Close();
            }

            return StatusCode(200, count);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }


}