using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using Bajuku_API.Models;

using System.Data;
using System.Security.Cryptography;
using System.Text;

// AUTHHH
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

using Bajuku_API.Methods;

namespace Bajuku_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private IConfiguration _configuration;
    private string _conString = "";
    public string jwtKey = "";
    public string jwtKeyEmail = "191ADD2CFB6FBA38AA7C87F8C63C7";
    private string _urlFront = "";

    public UsersController(IConfiguration configuration)
    {
        _configuration = configuration;
        _conString = configuration["ConnectionStrings:Default"];
        jwtKey = configuration["jwt:key"];
        _urlFront = configuration["Url:Frontend"];
    }

    [HttpGet]
    [Route("GetData")]
    public ActionResult GetData()

    {
        return StatusCode(200, new { salam = "hai" });
    }

    [HttpGet]
    [Route("TestToken")]
    [Authorize]
    public ActionResult TestToken()
    {
        try
        {
            return StatusCode(200, "Token Valid");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet]
    [Route("GetUsersAll")]
    public ActionResult GetUsersAll()
    {
        var result = new List<Users>();
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM Users";
                    cmd.Connection = conn;

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var tempUsers = new Users();

                            tempUsers.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                            tempUsers.email = reader["email"] == DBNull.Value ? "" : (string)reader["email"];
                            //tempUsers.username = reader["username"] == DBNull.Value ? "" : (string)reader["username"];
                            // tempUsers.password = reader["password"] == DBNull.Value ? "" : (string)reader["password"];tempUsers.first_name = reader["first_name"] == DBNull.Value ? "" : (string)reader["first_name"];
                            tempUsers.first_name = reader["first_name"] == DBNull.Value ? "" : (string)reader["first_name"];
                            tempUsers.last_name = reader["last_name"] == DBNull.Value ? "" : (string)reader["last_name"];
                            //tempUsers.address = reader["address"] == DBNull.Value ? "" : (string)reader["address"];
                            //tempUsers.telephone = reader["telephone"] == DBNull.Value ? "" : (string)reader["telephone"];
                            tempUsers.id_role = reader["id_role"] == DBNull.Value ? 0 : (int)reader["id_role"];

                            result.Add(tempUsers);
                        }
                    }

                    reader.Close();
                }

                conn.Close();
            }

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("Register")]
    public ActionResult Register([FromBody] UsersBody user)
    {
        try
        {
            // Validate Input Email
            if (Util.IsValidEmail(user.email) == false)
            {
                throw new Exception("Email tidak valid, silahkan input email dengan format yang benar.");
            }

            // Validate Input Password min 8 Character
            if (Util.IsValidPasswordFormat(user.password) == false)
            {
                throw new Exception("Password tidak valid, Minimal 8 Karakter, 1 Huruf, dan 1 Angka.");
            }

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;

                    // Check Email has Registered
                    cmd.CommandText = "SELECT * FROM Users WHERE email = @email";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@email", MySqlDbType = MySqlDbType.String, Value = user.email }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        throw new Exception("Email sudah dipakai, silahkan daftar dengan email yang belum terdaftar.");
                    }

                    reader.Close();

                    // Register User
                    cmd.CommandText = "INSERT INTO Users (id, email, password_hash, password_salt, first_name, last_name, id_role, `active`)" +
                                      "VALUES (@id, @email," +
                                              "@password_hash," +
                                              "@password_salt," +
                                              "@first_name," +
                                              "@last_name," +
                                              "@id_role, @active)";
                    byte[] salt;
                    byte[] hash;

                    using (var hmac = new HMACSHA512())
                    {
                        salt = hmac.Key;
                        hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(user.password));
                    }

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter {ParameterName="@id", MySqlDbType=MySqlDbType.Int32, Value = user.id},
            new MySqlParameter {ParameterName="@password_hash", MySqlDbType=MySqlDbType.Binary, Value = hash},
            new MySqlParameter {ParameterName="@password_salt", MySqlDbType=MySqlDbType.Binary, Value = salt},
            new MySqlParameter {ParameterName="@first_name", MySqlDbType=MySqlDbType.String, Value = user.first_name},
            new MySqlParameter {ParameterName="@last_name", MySqlDbType=MySqlDbType.String, Value = user.last_name},
            new MySqlParameter {ParameterName="@id_role", MySqlDbType=MySqlDbType.Int32, Value = user.id_role},
            new MySqlParameter {ParameterName="@active", MySqlDbType=MySqlDbType.Bool, Value = false}
                    });

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            List<Claim> claims = new List<Claim>
      {
        new Claim("#email", user.email),
        new Claim("#request_activation", "true")
      };

            string jwtToken = Token.CreateJwtToken(
              claims: claims,
              jwtKey: jwtKeyEmail
            );

            string verificationUrl = _urlFront + "/verification/" + jwtToken;

            string emailBody = @$"<body style=""margin: 0; padding: 0; box-sizing: border-box;"">
    <table align=""center"" cellpadding=""0"" cellspacing=""0"" width=""95%"">
      <tr>
        <td align=""center"">
          <table align=""center"" cellpadding=""0"" cellspacing=""0"" width=""600"" style=""border-spacing: 2px 5px;"" bgcolor=""#fff"">
            <tr>
              <td align=""center"" style=""padding: 5px 5px 5px 5px;"">
                <a href=""https://localhost:3000/"" target=""_blank"">
                  <img src=""https://i.ibb.co/ctzstJG/Logo.png"" alt=""Logo"" style=""width:420px; margin: -100px -100px; border:0;""/>
                </a>
              </td>
            </tr>
            <tr>
              <td bgcolor=""#fff"">
                <table cellpadding=""0"" cellspacing=""0"" width=""100%%"">
                  <tr>
                    <td style=""padding: 10px 0 10px 0; font-family: Nunito, sans-serif; font-size: 20px; font-weight: 900"">
                      Aktifkan Akun Bajuku Anda
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td bgcolor=""#fff"">
                <table cellpadding=""0"" cellspacing=""0"" width=""100%%"">
                  <tr>
                    <td style=""padding: 20px 0 20px 0; font-family: Nunito, sans-serif; font-size: 16px;"">
                      Hi, <span id=""name"">{user.email}</span>
                    </td>
                  </tr>
                  <tr>
                    <td style=""padding: 0; font-family: Nunito, sans-serif; font-size: 16px;"">
                      Terima kasih telah mendaftar di Bajuku. Mohon konfirmasi email ini untuk mengaktifkan akun Bajuku Anda.
                    </td>
                  </tr>
                  <tr>
                    <td style=""padding: 20px 0 20px 0; font-family: Nunito, sans-serif; font-size: 16px; text-align: center;"">
                      <a href=""{verificationUrl}"" style=""background-color: #39AB6D; text-decoration: none; border: none; color: white; padding: 15px 40px; text-align: center; display: inline-block; font-family: Nunito, sans-serif; font-size: 18px; font-weight: bold; cursor: pointer;"">
                        Konfimasi Email
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td style=""padding: 0; font-family: Nunito, sans-serif; font-size: 16px;"">
                      Jika Anda kesulitan mengklik tombol ""Konfirmasi Email"", copy dan paste URL di bawah ke dalam browser Anda:
                      <p id=""url"">{_urlFront}</p>
                    </td>
                  </tr>
                  <tr>
                    <td style=""padding: 50px 0; font-family: Nunito, sans-serif; font-size: 16px;"">
                      Regards,
                      <p>Bajuku Teams</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>";

            bool emailOk = Email.SendEmail(user.email, "VERIFY YOUR EMAIL - Bajuku", emailBody);

            if (!emailOk)
            {
                throw new Exception("Gagal mengirim email verifikasi, silahkan tunggu dan ulangi beberapa saat lagi.");
            }

            //

            return StatusCode(200, "Berhasil Register cek email mu untuk lanjut verifikasi");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("VerifyEmail/{token}")]
    public ActionResult VerifyEmail(string token)
    {
        try
        {
            // Verify Claim
            List<Claim> claims = Token.ParseJwtTokenClaim(token, jwtKeyEmail);

            bool isActive = false;
            string email = "";

            foreach (Claim claim in claims)
            {
                if (claim.Type == "#request_activation")
                {
                    isActive = true;
                }

                if (claim.Type == "#email")
                {
                    email = claim.Value;
                }
            }

            // Jika Claim Terverifkasi

            if (isActive == true && String.IsNullOrEmpty(email) == false)
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand())
                    {
                        // Aktivasi Status User
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE Users SET `active` = true WHERE email = @email";
                        cmd.Parameters.Add(
                          new MySqlParameter { ParameterName = "@email", MySqlDbType = MySqlDbType.String, Value = email }
                        );

                        cmd.Transaction = conn.BeginTransaction();

                        int result = cmd.ExecuteNonQuery();
                        if (result == -1)
                        {
                            cmd.Transaction.Rollback();
                            throw new Exception("Aktivasi user gagal");
                        }

                        cmd.Transaction.Commit();
                    }

                    conn.Close();
                }
            }
            else
            {
                throw new Exception("Verifikasi token gagal, silahkan tunggu dan ulangi beberapa saat lagi");
            }

            return StatusCode(200, "USER BERHASIL DIAKTIVASI, SILAHKAN LANJUT LOGIN DENGAN EMAIL DAN PASSWORD YANG SUDAH TERDAFTAR");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("Login")]
    public ActionResult Login([FromBody] UsersLogin user)
    {
        try
        {
            bool isRightUser = false;
            string jwtToken = "";
            int idUser = 0;
            int idRole = 2;

            // Validate Input Email
            if (Util.IsValidEmail(user.email) == false)
            {
                throw new Exception("Email tidak valid, silahkan input email dengan format yang benar.");
            }

            // Validate Input Password min 8 Character
            if (Util.IsValidPasswordFormat(user.password) == false)
            {
                throw new Exception("Password tidak valid, Minimal 8 Karakter, 1 Huruf, dan 1 Angka.");
            }

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM Users WHERE email = @email;";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@email", MySqlDbType = MySqlDbType.String, Value = user.email }
                    );
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            // Verifikasi user Aktif
                            bool active = (bool)reader["active"];

                            if (!active)
                            {
                                throw new Exception("Status akun belum aktif, silahkan lakukan aktivasi terlebih dahulu.");
                            }

                            // Verifikasi Password Hash
                            byte[] userHash = (byte[])reader["password_hash"];
                            byte[] userSalt = (byte[])reader["password_salt"];

                            using (var hmac = new HMACSHA512(userSalt))
                            {
                                byte[] checkHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(user.password));
                                isRightUser = checkHash.SequenceEqual(userHash);
                            }

                            if (!isRightUser)
                            {
                                throw new Exception("Email atau password salah");
                            }

                            if (isRightUser)
                            {
                                int role = (int)reader["id_role"];

                                List<Claim> claims = new List<Claim>
                {
                  new Claim(ClaimTypes.Name, user.email),
                  new Claim(ClaimTypes.Role, role.ToString())
                };

                                jwtToken = Token.CreateJwtToken(claims, jwtKey);
                                idUser = (int)reader["id"];
                                idRole = (int)reader["id_role"];
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Email atau password salah");
                    }

                    reader.Close();
                }

                conn.Close();
            }

            var result = new
            {
                jwtToken = jwtToken,
                id = idUser,
                role = idRole
            };

            return StatusCode(200, result);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpDelete]
    [Route("Delete")]
    public ActionResult UserDelete(int id)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "DELETE FROM users WHERE id = @id;";
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new MySqlParameter { ParameterName = "@id", DbType = DbType.Int32, Value = id });

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            return StatusCode(200, "Akun dengan id: " + id + " berhasil dihapus");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPut]
    [Route("Update")]
    public ActionResult UserUpdate(int id, [FromBody] UsersUpdate user)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "UPDATE Users SET " +
                                      "email = @email, " +
                                      "username = @username, " +
                                      "first_name = @first_name, " +
                                      "last_name = @last_name, " +
                                      "address = @address," +
                                      "telephone = @telephone," +
                                      "id_role = @id_role " +
                                      "WHERE id = @id;";
                    cmd.Connection = conn;
                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter { ParameterName = "@id", DbType = DbType.Int32, Value = id },
            new MySqlParameter { ParameterName = "@email", DbType = DbType.String, Value = (string)user.email },
            new MySqlParameter { ParameterName = "@username", DbType = DbType.String, Value = (string)user.username },
            new MySqlParameter { ParameterName = "@first_name", DbType = DbType.String, Value = (string)user.first_name },
            new MySqlParameter { ParameterName = "@last_name", DbType = DbType.String, Value = (string)user.last_name },
            new MySqlParameter { ParameterName = "@address", DbType = DbType.String, Value = (string)user.address },
            new MySqlParameter { ParameterName = "@telephone", DbType = DbType.String, Value = (string)user.telephone },
            new MySqlParameter { ParameterName = "@id_role", DbType = DbType.Int32, Value = (int)user.id_role }
                    });

                    cmd.ExecuteNonQuery();
                }

                conn.Close();

                return StatusCode(200, "Update Success");
            }
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("UpdateAccountType")]
    public ActionResult UpdateAccountType(int id, int id_role)
    {
        try
        {
            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE Users SET `id_role` = @id_role WHERE id = @id";
                    cmd.Parameters.AddRange(
                        new MySqlParameter[]
                        {
                                new MySqlParameter {ParameterName = "@id", MySqlDbType = MySqlDbType.Int32, Value = id},
                                new MySqlParameter {ParameterName = "@id_role", MySqlDbType = MySqlDbType.Int32, Value = id_role}
                        });

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }

                conn.Close();
            }

            return StatusCode(200);
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [Route("RegisterByAdmin")]
    public ActionResult RegisterByAdmin([FromBody] UsersBody user)
    {
        try
        {
            // Validate Input Email
            if (Util.IsValidEmail(user.email) == false)
            {
                throw new Exception("EMAIL TIDAK VALID");
            }

            // Validate Input Password min 8 Character
            if (Util.IsValidPasswordFormat(user.password) == false)
            {
                throw new Exception("PASSWORD TIDAK VALID");
            }

            using (var conn = new MySqlConnection(_conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;

                    // Check Email has Registered
                    cmd.CommandText = "SELECT * FROM Users WHERE email = @email";
                    cmd.Parameters.Add(
                      new MySqlParameter { ParameterName = "@email", MySqlDbType = MySqlDbType.String, Value = user.email }
                    );

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        throw new Exception("EMAIL ALREADY EXIST");
                    }

                    reader.Close();

                    // Register User
                    cmd.CommandText = "INSERT INTO Users (id, email, password_hash, password_salt, first_name, last_name, id_role, `active`)" +
                                      "VALUES (@id, @email," +
                                              "@password_hash," +
                                              "@password_salt," +
                                              "@first_name," +
                                              "@last_name," +
                                              "@id_role, @active)";
                    byte[] salt;
                    byte[] hash;

                    using (var hmac = new HMACSHA512())
                    {
                        salt = hmac.Key;
                        hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(user.password));
                    }

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
            new MySqlParameter {ParameterName="@id", MySqlDbType=MySqlDbType.Int32, Value = user.id},
            new MySqlParameter {ParameterName="@password_hash", MySqlDbType=MySqlDbType.Binary, Value = hash},
            new MySqlParameter {ParameterName="@password_salt", MySqlDbType=MySqlDbType.Binary, Value = salt},
            new MySqlParameter {ParameterName="@first_name", MySqlDbType=MySqlDbType.String, Value = user.first_name},
            new MySqlParameter {ParameterName="@last_name", MySqlDbType=MySqlDbType.String, Value = user.last_name},
            new MySqlParameter {ParameterName="@id_role", MySqlDbType=MySqlDbType.Int32, Value = user.id_role},
            new MySqlParameter {ParameterName="@active", MySqlDbType=MySqlDbType.Bool, Value = true}
                    });

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            return StatusCode(200, "Berhasil Register cek email mu untuk lanjut verifikasi");
        }
        catch (System.Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

}