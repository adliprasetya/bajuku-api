using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace Bajuku_API.Methods;

public static class Email
{
  
  public static bool SendEmail(string emailTo, string subjectText, string bodyHtml)
  {

    try
    {
      var email = new MimeMessage();
      email.From.Add(MailboxAddress.Parse("bajuku.teams@gmail.com"));
      email.To.Add(MailboxAddress.Parse(emailTo));
      email.Subject = subjectText;
      email.Body = new TextPart(TextFormat.Html) { Text = bodyHtml};

      using (var smtp = new SmtpClient())
      {
        smtp.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
        smtp.Authenticate("bajuku.teams@gmail.com", "wpibsprbomrjooub");
        smtp.Send(email);
        smtp.Disconnect(true);
      }

      return true;
    }
    catch (System.Exception)
    {
      
      throw;
    }
  }
}