using Bajuku_API.Models;

// CRYPTO AND AUTHHH
using System.Security.Cryptography;
using System.Text;
using System.Data;

// AUTHHH
using System.Security.Claims;

// DOWNLOAD AUTHHH
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;


namespace Bajuku_API.Methods;

public static class Token
{

  public static string CreateJwtToken(List<Claim> claims, string jwtKey)
  {

    // Create Credentials
    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
    SigningCredentials cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

    // Create JWT Token
    JwtSecurityToken token = new JwtSecurityToken(
      claims: claims,
      expires: DateTime.Now.AddDays(1),
      signingCredentials: cred
    );

    string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

    return jwtToken;
  }

  public static List<Claim> ParseJwtTokenClaim(string jwtToken, string jwtKey)
  {
    
    // Validate setting
    TokenValidationParameters validationParameters = new TokenValidationParameters
    {
      ValidateIssuerSigningKey = true,
      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey)),
      ValidateIssuer = false,
      ValidateAudience = false,
      ValidateLifetime = true,
      ClockSkew = TimeSpan.Zero,

    };

    // Get Claims
    SecurityToken validatedToken;
    ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(jwtToken, validationParameters, out validatedToken);

    return principal.Claims.ToList();
  }
}
