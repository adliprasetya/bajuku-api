using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Bajuku_API.Methods;

public static class Util
{

  public static bool IsValidEmail(string emailAddress)
  {

    if (String.IsNullOrEmpty(emailAddress))
    {
      return false;
    }

    try
    {
      
      MailAddress mail = new MailAddress(emailAddress);
      return true;
    }
    catch (FormatException)
    {
      return false;
    }
  }

  public static bool IsValidPasswordFormat(string text)
  {

    if (String.IsNullOrEmpty(text))
    {
      return false;
    }

    Regex r = new Regex(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$");

    if (r.IsMatch(text))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}